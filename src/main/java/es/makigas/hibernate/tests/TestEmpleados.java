package es.makigas.hibernate.tests;

import java.time.LocalDate;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import es.makigas.hibernate.modelo.Direccion;
import es.makigas.hibernate.modelo.Empleado;

public class TestEmpleados {	
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Persistencia");
	
	
	public static void main(String[] args) {
		
//		EntityManager manager = emf.createEntityManager();
//		Empleado e = new Empleado(10L,"Perez","Pepito",LocalDate.of(1979,6,6));
//		
//		//error, the address doesnt exist, you cant change something null, it is not saved
		//only perform with cascade = cascadetype.all
//		e.setDireccion(new Direccion(15L,"calle falsa, 123", "Sprinfield","Springfield2", "USA"));
//		manager.getTransaction().begin();
//		manager.persist(e);
//		manager.getTransaction().commit();
//		manager.close();
//		
//		imprimirTodo();
		
		EntityManager manager = emf.createEntityManager();
		
		Direccion d = new Direccion(15L,"calle falsa, 123", "Sprinfield","Springfield2", "USA");
		Empleado e = new Empleado(10L,"Perez","Pepito",LocalDate.of(1979,6,6));
		
		//first create address then we can save the information
		e.setDireccion(d);
		manager.getTransaction().begin();
		manager.persist(d);
		manager.persist(e);
		manager.getTransaction().commit();
		manager.close();
		
		imprimirTodo();
	}
	
	public static void imprimirTodo() {
		EntityManager manager = emf.createEntityManager();
		List<Empleado> emps = (List<Empleado>) manager.createQuery("FROM Empleado").getResultList();
		System.out.println("Hoy "+emps.size() + "empleados en el sistema.");
		for(Empleado emp : emps) {
			System.out.println(emp.getNombre() + "," + emp.getApellidos() + "," + emp.getDireccion().toString() );
		}
	}

}
