package es.makigas.hibernate.modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
//todo standar y no depender de hibernate
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="EMPLEADO")
public class Empleado implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="COD_EMPLEADO")
	private Long codigo;
	
	@Column(name="APELLIDOS")
	private String apellidos;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="FECHA_NACIMIENTO")
	private LocalDate fechaNacicmiento;
	
	//delete and create one to one relation
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="ID_DIRECCION")//the employee is the owner and join with the direction automaticaly
	private Direccion direccion;
	
	public Empleado() {}
	
	public Empleado(Long codigo, String apellidos, String nombre, LocalDate fechaNacicmiento) {
		super();
		this.codigo = codigo;
		this.apellidos = apellidos;
		this.nombre = nombre;
		this.fechaNacicmiento = fechaNacicmiento;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaNacicmiento() {
		return fechaNacicmiento;
	}

	public void setFechaNacicmiento(LocalDate fechaNacicmiento) {
		this.fechaNacicmiento = fechaNacicmiento;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}
	
	
}
